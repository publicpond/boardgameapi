package com.quack.boardgameapi.data;


import com.quack.boardgameapi.service.persistence.GameLocalPersistenceEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    @Value("${spring.datasource.url}")
    private String connectionURL;
    @Value("${spring.datasource.username}")
    private String connectionUserName;
    @Value("${spring.datasource.password}")
    private String connectionPassword;
    private static Database instance;
    public static Database getInstance() {
        if (instance == null) {
            instance = new Database();
        }
        return instance;
    }
    private Database() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            GameLocalPersistenceEngine.LOGGER.warn("database connection failed " + e.toString());

        }
    }

    private Connection connection;

    public Connection getConnection() throws SQLException {
        if (connection == null || connection.isClosed()) {//"jdbc:mysql://localhost:6603/BoardGameDatabase""root"
            connection = DriverManager.getConnection(connectionURL, connectionUserName, connectionPassword);
        }
        return connection;
    }
}
