package com.quack.boardgameapi.entity;

import fr.le_campus_numerique.square_games.engine.Token;
import jakarta.annotation.Nullable;
import jakarta.persistence.*;
import lombok.*;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.UUID;

@Entity
@Getter
@Setter
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class TokenPositionEntity implements DataEntity {
    @Id
    @GeneratedValue
    private Long id;
    @OneToOne(fetch = FetchType.EAGER)
    private UserEntity owner;
    @NotNull
    private String tokenName;
    private int x;
    private int y;

}
