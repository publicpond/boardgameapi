package com.quack.boardgameapi.entity;

import jakarta.persistence.*;
import lombok.*;
import java.util.Collection;

@Entity
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class GameSaveEntity implements DataEntity {
    @Id
    @GeneratedValue
    private Long id;
    private String gameName;
    private int boardSize;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private Collection<TokenPositionEntity> boardTokens;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.PERSIST)
    private Collection<TokenPositionEntity> removedTokens;
    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    private Collection<UserEntity> players;
}
