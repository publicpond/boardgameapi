package com.quack.boardgameapi.entity;

import jakarta.persistence.*;
import lombok.*;
import org.springframework.security.core.userdetails.UserDetails;

import javax.validation.constraints.NotNull;
import java.util.Collection;
import java.util.UUID;

@Entity
@Getter
@Setter
@Data
@NoArgsConstructor
@RequiredArgsConstructor
@AllArgsConstructor
@Builder
public class UserEntity implements UserDetails {

    @Id
    private UUID uuid;
    private @NotNull String username;
    private @NotNull String password;
    private @NotNull String email;
    @OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.MERGE)
    private Collection<AuthorityEntity> authorities;
    @ManyToMany(fetch = FetchType.EAGER)
    private Collection<GameSaveEntity> saves;
    @OneToMany(cascade = CascadeType.ALL)
    private Collection<TokenPositionEntity> tokens;
    @Override
    public boolean isAccountNonExpired() {
        return false;
    }
    @Override
    public boolean isAccountNonLocked() {
        return false;
    }
    @Override
    public boolean isCredentialsNonExpired() {
        return false;
    }
    @Override
    public boolean isEnabled() {
        return false;
    }
}
