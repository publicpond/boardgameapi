# *Code en anglais*

# Variables
```java
// Constantes en majuscule
final String MA_CONSTANTE = "A";

// Variables en camelCase sans underscore
# OK 
String myVariable;
# Pas OK
String ma_variable_baguette;
```

# Methods
Les méthodes doivent au minimum annoncer ce qu'elles font et doivent être écrites en camelcase

# Classes
Les noms de classes doivent toutes commencer par une majuscule

Correct: 

>User.java
```java
class MyUserClass {
    
}
```
Pas Correct:
```java
class ma_classe_User {
    
}
```
Pour certaines annotations on va préférer nommer la classe en ajoutant le nom de l'annotation

```java
import org.springframework.web.bind.annotation.RestController;

@Entity
class AppleEntity {

}

@RestController
class AppleController {
    
}
```
# Packages
Packages et sous packages en minuscule